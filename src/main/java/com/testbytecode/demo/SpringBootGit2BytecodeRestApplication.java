package com.testbytecode.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootGit2BytecodeRestApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootGit2BytecodeRestApplication.class, args);
	}
}
