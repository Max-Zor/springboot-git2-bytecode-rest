package com.testbytecode.demo.repositorio;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.testbytecode.demo.entities.Vehiculo;

@Repository
public interface Repo extends JpaRepository<Vehiculo, Long>{
	
	public Vehiculo findByCarro(String carro);
	public Vehiculo findByMarca(String marca);
	public Vehiculo findByTransmision(String transmision);
	public Vehiculo findByCarroAndId(String carro, long id);

}
