package com.testbytecode.demo.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Vehiculo {

	@Id
	@GeneratedValue
	private long id;
	private String carro;
	private String marca;
	private String transmision;

	public Vehiculo() {

	}

	public Vehiculo(long id, String carro, String marca, String transmision) {
		this.id = id;
		this.carro = carro;
		this.marca = marca;
		this.transmision = transmision;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getCarro() {
		return carro;
	}

	public void setCarro(String carro) {
		this.carro = carro;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getTransmision() {
		return transmision;
	}

	public void setTransmision(String transmision) {
		this.transmision = transmision;
	}

}
